
// START TÍNH ĐIỂM THI
function tinhTongBaMon(){
    var subject1Score = document.getElementById("firstMark").value * 1;
    var subject2Score = document.getElementById("secondMark").value * 1;
    var subject3Score = document.getElementById("thirdMark").value * 1;
    var tinhTongBaMon = subject1Score + subject2Score + subject3Score;
    return tinhTongBaMon;
}

function diemCongKhuVuc(khuVuc){
    var tongDiem1 = tinhTongBaMon();

    if (khuVuc == "KV1"){
        tongDiem1 += 0.75;
    }else if(khuVuc == "KV2"){
        tongDiem1 += 0.25;
    }else if(khuVuc == "KV2NT"){
        tongDiem1 += 0.5;
    } else if(khuVuc == "KV3"){
        tongDiem1 += 0;
    }
    return tongDiem1;
}

function diemCongDoiTuong(doiTuong){
    var tongDiem2 = tinhTongBaMon();

    if(doiTuong == 0){
        tongDiem2 += 0;       
    }else if(doiTuong == "UT1"){
        tongDiem2 += 2;
    }else if(doiTuong == "UT2"){
        tongDiem2 += 1;
    }
    return tongDiem2;
}

function tinhDiemTuyenSinh(){
    var diemChuan = document.getElementById("benchMark").value * 1;
    
    var khuvucValue = document.getElementById("areaSelect").value * 1;
    var doituongValue = document.getElementById("prioritySelect").value * 1;

    var subject1Score = document.getElementById("firstMark").value * 1;
    var subject2Score = document.getElementById("secondMark").value * 1;
    var subject3Score = document.getElementById("thirdMark").value * 1;

    var tongDiem = tinhTongBaMon();

    var tongDiemCongKhuVuc = diemCongKhuVuc(khuvucValue);
    var tongDiemCongDoiTuong = diemCongDoiTuong(doituongValue);
    
    var diemTongSauKhiCong = tongDiemCongKhuVuc + tongDiemCongDoiTuong - tongDiem;

    if(subject1Score === 0 || subject2Score === 0 || subject3Score === 0){
        document.getElementById("result1").innerHTML = `<p>Bạn đã rớt do có điểm bằng 0</p>`
    }else if(diemTongSauKhiCong >= diemChuan){
        document.getElementById("result1").innerHTML = `<p>Chúc mừng bạn đã đỗ. Tổng điểm của bạn là: ${diemTongSauKhiCong}</p>`
    }else{
        document.getElementById("result1").innerHTML = `<p>Bạn đã rớt</p>`
    }
}
// END TÍNH ĐIỂM THI

// START TÍNH TIỀN ĐIỆN
const PRICE_KM1_KM50 = 500;
const PRICE_KM50_KM100 = 650;
const PRICE_KM100_KM200 = 850;
const PRICE_KM200_KM350 = 1100;
const PRICE_KM350_TRODI = 1300;

function tinhTienDien(){
    var tenKhachHang = document.getElementById("customerName").value;
    var kWDien = document.getElementById("kwNumber").value * 1;

    var tongTienDien;

    if(kWDien >= 1 && kWDien <=50){
        tongTienDien = kWDien * PRICE_KM1_KM50
    }else if(kWDien > 50 && kWDien <=100){
        tongTienDien = PRICE_KM1_KM50 * 50 + (kWDien-50)*PRICE_KM50_KM100;
    }else if(kWDien >100 && kWDien <= 200){
        tongTienDien = PRICE_KM1_KM50 * 50 + (kWDien-50)*PRICE_KM50_KM100 + (kWDien - 100) * PRICE_KM100_KM200;
    }else if(kWDien > 200 && kWDien <=350){
       tongTienDien = PRICE_KM1_KM50 * 50 + (kWDien-50)*PRICE_KM50_KM100 + (kWDien - 100) * PRICE_KM100_KM200 + (kWDien - 200) * PRICE_KM200_KM350;
    }else{
        tongTienDien = PRICE_KM1_KM50 * 50 + (kWDien-50)*PRICE_KM50_KM100 + (kWDien - 100) * PRICE_KM100_KM200 + (kWDien - 200) * PRICE_KM200_KM350 + (kWDien - 350) * PRICE_KM350_TRODI;
    }

    document.getElementById("tienDien").innerHTML = `<p>Họ tên: ${tenKhachHang}. Tiền điện: ${tongTienDien}`;
}

// TÍNH TIỀN CÁP
function myFunction() {
    var x = document.getElementById("mySelect__03").value;
    if (x == "ND") {
      document.getElementById("connection __input").style.display = "none";
    } else if (x == "DN") {
      document.getElementById("connection __input").style.display = "block";
      document.getElementById("connection __input").style.marginTop = "13px";
    }
  }
  
  function tinhTienCap() {
    var maKhachHang = document.getElementById("code__input").value;
    var selectValue = document.getElementById("mySelect__03");
    var valueKhuVuc = selectValue.value;
    var soKenhCaoCap = document.getElementById("channel__input").value*1;
    var soKetNoi = document.getElementById("connection __input").value*1;
    
    var tienCap;
  
    if (valueKhuVuc == "ND") {
      tienCap =  4.5 + soKenhCaoCap * 7.5 + 20.5;
    } else if (valueKhuVuc == "DN") {
      if(soKetNoi <= 10) {
        tienCap = 75 + (soKenhCaoCap * 50) + 15;
      } else {
        tienCap = 75 + (soKetNoi - 10) * 5 + soKenhCaoCap * 50 + 15;
      }
    }
   document.getElementById("result__04").innerHTML = `<p>Mã khách hàng: ${maKhachHang}<br>Tiền cáp: ${tienCap} $</p>`;
  }